PGDMP         &                x            ITIS_booking    10.12    10.12                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            	           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            
           1262    16519    ITIS_booking    DATABASE     �   CREATE DATABASE "ITIS_booking" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE "ITIS_booking";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false                       0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16702    reservations    TABLE     �   CREATE TABLE public.reservations (
    id integer NOT NULL,
    id_user integer NOT NULL,
    room integer NOT NULL,
    date date NOT NULL,
    "time" time without time zone NOT NULL,
    comment text
);
     DROP TABLE public.reservations;
       public         postgres    false    3            �            1259    16700    reservations_id_seq    SEQUENCE     �   ALTER TABLE public.reservations ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.reservations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    200    3            �            1259    16695    rooms    TABLE     9   CREATE TABLE public.rooms (
    room integer NOT NULL
);
    DROP TABLE public.rooms;
       public         postgres    false    3            �            1259    16562    users    TABLE     �   CREATE TABLE public.users (
    id integer NOT NULL,
    name text NOT NULL,
    email text NOT NULL,
    CONSTRAINT proper_email CHECK ((email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$'::text))
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1259    16560    users_id_seq    SEQUENCE     �   ALTER TABLE public.users ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    197    3                      0    16702    reservations 
   TABLE DATA               P   COPY public.reservations (id, id_user, room, date, "time", comment) FROM stdin;
    public       postgres    false    200   �                 0    16695    rooms 
   TABLE DATA               %   COPY public.rooms (room) FROM stdin;
    public       postgres    false    198   �                 0    16562    users 
   TABLE DATA               0   COPY public.users (id, name, email) FROM stdin;
    public       postgres    false    197   �                  0    0    reservations_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.reservations_id_seq', 1, false);
            public       postgres    false    199                       0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 4, true);
            public       postgres    false    196            �
           2606    16709    reservations reservations_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.reservations DROP CONSTRAINT reservations_pkey;
       public         postgres    false    200            �
           2606    16711 ,   reservations reservations_room_date_time_key 
   CONSTRAINT     u   ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_room_date_time_key UNIQUE (room, date, "time");
 V   ALTER TABLE ONLY public.reservations DROP CONSTRAINT reservations_room_date_time_key;
       public         postgres    false    200    200    200            �
           2606    16699    rooms rooms_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT rooms_pkey PRIMARY KEY (room);
 :   ALTER TABLE ONLY public.rooms DROP CONSTRAINT rooms_pkey;
       public         postgres    false    198            |
           2606    16572    users users_name_key 
   CONSTRAINT     O   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_name_key UNIQUE (name);
 >   ALTER TABLE ONLY public.users DROP CONSTRAINT users_name_key;
       public         postgres    false    197            ~
           2606    16570    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    197            �
           2606    16712 &   reservations reservations_id_user_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_id_user_fkey FOREIGN KEY (id_user) REFERENCES public.users(id);
 P   ALTER TABLE ONLY public.reservations DROP CONSTRAINT reservations_id_user_fkey;
       public       postgres    false    200    197    2686            �
           2606    16717 #   reservations reservations_room_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_room_fkey FOREIGN KEY (room) REFERENCES public.rooms(room);
 M   ALTER TABLE ONLY public.reservations DROP CONSTRAINT reservations_room_fkey;
       public       postgres    false    198    200    2688                  x������ � �         -   x�3410�2410� �D��3a",@�%�04������ U�         ?   x�3�t,*�I��/���L,*v�.H+�+*�2�tL*J�-ήJ�d��f�&�g����qqq �N�     